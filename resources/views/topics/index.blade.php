@extends('layouts.app')

@section('content')

    <div class="container">
        <h5><a href="/topics/create">Create Topic</a></h5>

        <h2>Topic List</h2>
    
        <hr>
    
        @if (count($topics) == 0)
    
            <h3>No Topics Created Yet</h3>
    
        @else
    
            @foreach ($topics as $topic)
                <p><strong><a href="/topics/{{$topic->id}}">{{$topic->title}}</a></strong></p>
    
                @if ($topic->state == ucfirst('open'))
                    <p class="text-success">Open</p>
                @else
                    <p class="text-danger">Closed</p>
                @endif
    
                <small><strong>Created: </strong>{{$topic->created_at}}</small>

                @if ($topic->user_id == Auth::user()->id && $topic->state == 'Open')

                    <br>
                    
                    <form action="/topics/{{$topic->id}}" method="post">
                        @csrf
                        @method('PUT')

                        <input type="submit" value="Close" class="btn btn-danger btn-sm">

                    </form>
                @endif

                @if ($topic->user_id == Auth::user()->id && $topic->state == 'Closed')

                    <br>

                    <form action="/topics/{{$topic->id}}" method="post">
                        @csrf
                        @method('PUT')

                        <input type="submit" value="Open" class="btn btn-success btn-sm">

                    </form>

                @endif

                <hr>
    
            @endforeach
        @endif
    </div>

@endsection