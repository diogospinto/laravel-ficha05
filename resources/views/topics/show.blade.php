@extends('layouts.app')

@section('content')
    
    <div class="container">

        <h4><a href="/topics">Topic List</a></h4>

        <h3>{{$topic->title}}</h3>
        <small><strong>Created: </strong>{{$topic->created_at}}</small>
        <br>
        <br>

        <p>{{$topic->description}}</p>

        @if ($topic->state == ucfirst('open'))
            <h5 class="text-success">Open</h5>
        @else
            <h5 class="text-danger">Closed</h5>
        @endif

        @if ($topic->user_id == Auth::user()->id && $topic->state == 'Open')
            
            <form action="/topics/{{$topic->id}}" method="post">
                @csrf
                @method('PUT')

                <input type="submit" value="Close" class="btn btn-danger btn-sm">

            </form>
            
            <br>

        @endif

        @if ($topic->user_id == Auth::user()->id && $topic->state == 'Closed')
            
            <form action="/topics/{{$topic->id}}" method="post">
                @csrf
                @method('PUT')

                <input type="submit" value="Open" class="btn btn-success btn-sm">

            </form>
            
            <br>

        @endif

        @if ($topic->state != 'Closed')
            <form action="/topics/{{$topic->id}}/answers" method="post">
                @csrf

                <h4>Add New Comment</h4>

                <textarea name="body" cols="50" rows="5" style="resize: none" required></textarea>
                <br>

                <input type="submit" value="Add" class="btn btn-primary btn-sm">

            </form>

            <br>
        @endif
    
        @if (count($answers) == 0)
            <p>No comments! </p>
        @else
            @if (count($answers) == 1)
                <p>{{count($answers)}} Comment</p>
            @else
                <p>{{count($answers)}} Comments</p>
            @endif
        @endif

        <hr>

        @foreach ($answers as $answer)

            <small>{{$answer->created_at}}</small>
            <p>{{$answer->body}}</p>
            <hr>

        @endforeach

    </div>

@endsection