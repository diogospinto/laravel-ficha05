@extends('layouts.app')

@section('content')
    
    <div class="container">

        <h5><a href="/topics">Topic List</a></h5>

        <form action="/topics" method="post">
            @csrf

            <h3 class="text-success">New Topic</h3>

            <h4>Title</h4>
            <input type="text" name="title" required>
            <br>

            <h4>Description</h4>
            <textarea name="description" cols="50" rows="5" style="resize: none"></textarea>
            <br>
            <br>

            <input type="submit" value="Create" class="btn btn-primary btn-sm">
        
        </form> 
    </div>

@endsection