<?php

namespace App\Http\Controllers;

use App\Topic;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Answer;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = Topic::orderBy('created_at', 'Desc')->get();

        return view('topics.index')->with(compact('topics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('topics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $topic = new Topic();

        $topic->title = $request->title;
        $topic->description = $request->description;
        $topic->state = 'Open';
        $topic->user_id = Auth::user()->id;

        $topic->save();

        return redirect('/topics');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function show(Topic $topic)
    {
        $topic = Topic::findOrFail($topic->id);
        $answers = Answer::orderBy('created_at', 'Desc')->where('topic_id', $topic->id)->get();

        return view('topics.show')->with(compact('topic', 'answers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function edit(Topic $topic)
    {
        $topic = Topic::findOrFail($topic->id);

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Topic $topic)
    {
        $topic = Topic::findOrFail($topic->id);

        if ($topic->state == 'Closed') {
            $topic->state = 'Open';
        } else if ($topic->state == 'Open') {
            $topic->state = 'Closed';
        }

        $topic->save();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Topic $topic)
    {
        //
    }
}
